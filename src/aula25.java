public class aula25 {  // operadores de atribuição

    public static void main(String[] args) {

        /*int i = 0;  // variavel inteira valor 0

        i = i + 1; // i deve somar + 1 ao valor e retornar
        System.out.println( i );
        */

        int x = 12;
        int y = 4;

        /*
        * x += y;
        * x -= y;
        *
        * x *= y;
        * x / y;
        *
        * x %= y;
        *
         */

        x = 12;  // valor de x
        System.out.println(x += y);  // soma

        x =12;
        System.out.println(x -= y);  // subtração

        System.out.println("--------");

        x =12;
        System.out.println(x *= y); // multiplicação

        x =12;
        System.out.println(x /= y); // divisão

        System.out.println("--------");

        x =12;
        System.out.println(x %= y);  // resto da divisão


    }
}
